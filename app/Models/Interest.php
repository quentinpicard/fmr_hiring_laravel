<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
    protected	$table		=	'interests';
	public 		$timestamps =	false;


    // Relation
	public function details()
	{
		return $this->hasMany('\App\Models\Interests\InterestDetail', 'interest_id', 'id');
	}


	public function user()
	{
		return $this->belongsTo('\App\User');
	}
}
