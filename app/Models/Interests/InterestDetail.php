<?php

namespace App\Models\Interests;

use Illuminate\Database\Eloquent\Model;

class InterestDetail extends Model
{
	protected 	$table 			=	'interests_details';
	public		$timestamps		=	false;



	// Mutators
	public function setSlugAttribute($value) {
		$this->attributes['slug']	=	str_slug($value);
	}

}
