<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Interest;
use Auth;
use File;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];




    // Mutator
	public function setPasswordAttribute($value)
	{
		$this->attributes['password']	=	bcrypt($value);
	}



    // Relations
	public function interests()
	{
		return $this->belongsToMany('\App\Models\Interest', 'users_interests', 'user_id', 'interest_id', 'user_id');
	}

    public static function is_interested($product){
        $json = File::get("../database/files/products_descriptions.json");
        $text = json_decode($json, true)[$product];
        $user = Auth::user();
        $count = 0;
        $allDetails = True;
        foreach ($user->interests as $interest) {
            foreach ($interest->details as $detail) {
                $count += substr_count($text, $detail->slug);
                if(strpos($text, $detail->slug) === false && $allDetails != False){
                    $allDetails = False;
                }
            }
        }
        if($allDetails){
            return 100;
        }else {
            if($count > 0){
                if($count == 1) {
                    return 60;
                }
                else{
                    $temp = 40/($count);
                    return 60 + ($count-1) * $temp;
                }
            }
            else {
                return 0;
            }
        }
    }
}
