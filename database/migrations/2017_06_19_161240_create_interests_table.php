<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interests', function(BluePrint $table)
		{
			$table 	->increments('id');
			$table 	->string('code');
		});

        Schema::create('interests_details', function(BluePrint $table)
		{
			$table 	->increments('id');

			$table 	->string('slug');
			$table 	->integer('interest_id')->unsigned();


			// Relations
			$table 	->foreign('interest_id')
					->references('id')->on('interests');
		});

        Artisan::call('db:seed', [
			'--class' => 'UsersInterests',
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interests_details');
        Schema::dropIfExists('interests');
    }
}
