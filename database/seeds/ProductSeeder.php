<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get("database/files/products_descriptions.json");
        $data = json_decode($json);
        $faker = Faker::create();
        foreach ($data as $name => $desc) {
            DB::table('products')->insert([
                'name' => $name,
                'order_id' => $faker->numberBetween(1, 2),
                'price' => $faker->numberBetween(50, 1000),
                'created_at' => date('Y-m-d'),
            ]);
        }
    }
}
