<?php

use Illuminate\Database\Seeder;

// Models
use \App\Models\Interest;
use \App\Models\Interests\InterestDetail;

class UsersInterests extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $interests	=	[
        	'alcool'	=>	[
        		'bière', 'alcool', 'vin'
			],
			'surf'	=>	[
				'surf', 'sup', 'stand up paddle', 'vague', 'eau', 'mer'
			],
			'fromage'	=>	[
				'fromage', 'parmesan', 'gruyère'
			],
			'vis piton'	=>	[
				'vis', 'piton'
			]
		];


        foreach ($interests as $code=>$interests_details)
		{
			// Save interest
			$interest	=	Interest::where('code', str_slug($code))->first();
			if (!$interest) {
				$interest = new Interest();
				$interest->code = str_slug($code);
				$interest->save();
			}


			// Save details of this interests
			foreach ($interests_details as $detail_)
			{
				$detail	=	InterestDetail::where('slug', str_slug($detail_))->first();
				if (!$detail)
				{
					$detail	=	new InterestDetail();
					$detail->slug			=	$detail_;
					$detail->interest_id	=	$interest->id;
					$detail->save();
				}
			}
		}
    }
}
