<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user	=	\App\User::where('email', 'root@root.fr')->first();
        if (!$user)
		{
			$user	=	new \App\User();
			$user->email 	=	'root@root.fr';
			$user->password =	'Azerty123';
			$user->firstname	=	'Adde';
			$user->lastname 	=	'Mine';
			$user->uniq_token	=	bcrypt(uniqid());

			$user->save();
		}
    }
}
